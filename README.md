# Melanoma Simulator

Simulator of an API REST for a melanoma classificator. It returns random relusts, there is not a melanoma detector in the project.

## Startup

You will need docker and docker-compose to be installed in your computer.

Run the docker compose file from the project main directory:  
```shell
docker-compose up
```

## Usage

This simulator publishes the next functions:   

| Type | URL                    | Parameters        | Result |
|------|------------------------|-------------------|--------|
| POST | /check_melanoma        | Image attatchment | JSON * |
| GET  | /files/last_result.jpg | None              | Image  |  

(*) {"malignity": float, "diagnostic": string, "image_link": url link to resulting image}